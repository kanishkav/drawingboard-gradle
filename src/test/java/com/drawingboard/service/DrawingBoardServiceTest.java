package com.drawingboard.service;

import com.drawingboard.dao.DrawingBoardDAO;
import com.drawingboard.dto.DrawingBoardDTO;
import com.drawingboard.dto.DrawingBoardDetailedDTO;
import com.drawingboard.dto.ShapeWrapperDTO;
import com.drawingboard.dto.ShapeWrapperMinimalDTO;
import com.drawingboard.exception.ResourceNotFoundException;
import com.drawingboard.mapper.DrawingBoardMapper;
import com.drawingboard.mapper.DrawingBoardMapperImpl;
import com.drawingboard.model.Circle;
import com.drawingboard.model.Shape;
import com.drawingboard.repository.IDrawingBoardRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class DrawingBoardServiceTest {
    @Mock
    private IDrawingBoardRepository drawingBoardRepository;
    @Mock
    private DrawingBoardMapper mapper;
    @Mock
    private IShapeService shapeService;
    private DrawingBoardService drawingBoardService;
    private DrawingBoardMapperImpl map;
    private int boardId;

    @BeforeEach
    void setUp() {
        drawingBoardService = new DrawingBoardService(drawingBoardRepository, mapper);
        drawingBoardService.setShapeService(shapeService);
        map = new DrawingBoardMapperImpl();
        boardId = 3;
    }


    @Test
    void canGetAllDrawingBoards() {
        drawingBoardService.getAll();

        verify(drawingBoardRepository).getAll();
    }

    @Test
    void canGetDrawingBoardIfExists() {
        int boardId = 3;
        DrawingBoardDAO board = new DrawingBoardDAO();
        given(drawingBoardRepository.getbyId(boardId)).willReturn(board);
        DrawingBoardDTO recievedBoard = drawingBoardService.getById(boardId);

        ArgumentCaptor<Integer> argumentCaptor = ArgumentCaptor.forClass(Integer.class);
        verify(drawingBoardRepository).getbyId(argumentCaptor.capture());

        assertThat(argumentCaptor.getValue()).isEqualTo(boardId);
        assertThat(recievedBoard).isEqualTo(mapper.boardDAOtoboardDTO(board));

    }

    @Test
    void throwResourceNotFoundIfNotExists() {
        given(drawingBoardRepository.getbyId(boardId)).willReturn(null);


        assertThatThrownBy(() -> drawingBoardService.getById(boardId)).isInstanceOf(ResourceNotFoundException.class);

        ArgumentCaptor<Integer> argumentCaptor = ArgumentCaptor.forClass(Integer.class);
        verify(drawingBoardRepository).getbyId(argumentCaptor.capture());

        assertThat(argumentCaptor.getValue()).isEqualTo(boardId);

    }

    @Test
    void add() {
        DrawingBoardDAO board = getADrawingBoard();

        DrawingBoardDTO boardDTO = map.boardDAOtoboardDTO(board);
        given(mapper.boardDTOtoboardDAO(boardDTO)).willReturn(board);
        given(drawingBoardRepository.add(board)).willReturn(boardId);
        board.setId(boardId);

        drawingBoardService.add(boardDTO);
        ArgumentCaptor<DrawingBoardDAO> argumentCaptor = ArgumentCaptor.forClass(DrawingBoardDAO.class);
        verify(drawingBoardRepository).add(argumentCaptor.capture());

        assertThat(argumentCaptor.getValue()).isEqualTo(board);
        assertThat(boardDTO.getId()).isEqualTo(boardId);

    }

    @Test
    void getRunTimeErrorWhenTryingToUpdateWithDifferentId() {
        int boardId2 = 4;
        DrawingBoardDAO board = getADrawingBoard();

        DrawingBoardDTO boardDTO = map.boardDAOtoboardDTO(board);

        verify(drawingBoardRepository, never()).update(any());
        assertThatThrownBy(() -> drawingBoardService.update(boardDTO, boardId2))
                .isInstanceOf(RuntimeException.class)
                .hasMessageContaining("Bad Request id doesnt match");

    }

    @Test
    void getResourceNotFoundErrorWhenNoBoardFound() {
        DrawingBoardDAO board = getADrawingBoard();

        DrawingBoardDTO boardDTO = map.boardDAOtoboardDTO(board);
        given(drawingBoardRepository.getbyId(boardId)).willReturn(null);

        verify(drawingBoardRepository, never()).update(any());
        assertThatThrownBy(() -> drawingBoardService.update(boardDTO, boardId))
                .isInstanceOf(ResourceNotFoundException.class);
    }

    @Test
    void updateDrawingBoardWhenFound() {

        DrawingBoardDAO board = getADrawingBoard();
        DrawingBoardDTO boardDTO = map.boardDAOtoboardDTO(board);
        given(drawingBoardRepository.getbyId(boardId)).willReturn(board);

        ArgumentCaptor<DrawingBoardDAO> argumentCaptor = ArgumentCaptor.forClass(DrawingBoardDAO.class);
        drawingBoardService.update(boardDTO, boardId);
        verify(drawingBoardRepository).update(argumentCaptor.capture());
        assertThat(argumentCaptor.getValue()).isEqualTo(board);

    }

    @Test
    void returnResourceNotFoundExceptionWhenDeletingNonExistingBoard() {
        given(drawingBoardRepository.getbyId(boardId)).willReturn(null);
        verify(drawingBoardRepository, never()).deleteById(boardId);
        assertThatThrownBy(() -> drawingBoardService.deleteById(boardId))
                .isInstanceOf(ResourceNotFoundException.class);

    }

    @Test
    void canWhenDeletingExistingBoard() {
        ArgumentCaptor<Integer> argumentCaptor = ArgumentCaptor.forClass(Integer.class);
        given(drawingBoardRepository.getbyId(boardId)).willReturn(new DrawingBoardDAO());
        drawingBoardService.deleteById(boardId);
        verify(drawingBoardRepository).deleteById(argumentCaptor.capture());
        assertThat(argumentCaptor.getValue()).isEqualTo(boardId);

    }

    @Test
    void throwErrorGettingDetailedBoardOfNonExistingBoard() {
        given(drawingBoardRepository.getbyId(boardId)).willReturn(null);
        verify(shapeService, never()).getShapesByBoardId(boardId);
        assertThatThrownBy(() -> drawingBoardService.getDetailedBoardById(boardId))
                .isInstanceOf(ResourceNotFoundException.class);
    }

    @Test
    void canGetDetailedBoardOfExistingBoard() {
        DrawingBoardDAO board = getADrawingBoard();

        ShapeWrapperDTO shape = ShapeWrapperDTO.builder()
                .shapeName(Shape.CIRCLE.getName())
                .drawingBoardId(boardId)
                .id(1)
                .attributes(new Circle(2))
                .startX(1)
                .startY(1)
                .selected(true)
                .backGroundColor("blue")
                .lineThickness(1)
                .build();

        DrawingBoardDTO baordDto = map.boardDAOtoboardDTO(board);

        DrawingBoardDetailedDTO detailedBoard = new DrawingBoardDetailedDTO();
        detailedBoard.setBoard(map.boardDAOtoboardDTO(board));
        List<ShapeWrapperDTO> shapeList = new ArrayList<>();
        shapeList.add(shape);
        List<ShapeWrapperMinimalDTO> shapeWrapperMinimalDTOS = map.shapewrapperlistDTOtominimalDTO(shapeList);


        detailedBoard.setShapes(shapeWrapperMinimalDTOS);
        given(shapeService.getShapesByBoardId(boardId)).willReturn(shapeList);
        given(mapper.shapewrapperlistDTOtominimalDTO(shapeList)).willReturn(shapeWrapperMinimalDTOS);
        given(drawingBoardRepository.getbyId(boardId)).willReturn(board);
        given(mapper.boardDAOtoboardDTO(board)).willReturn(baordDto);
        Object returnType = drawingBoardService.getDetailedBoardById(boardId);
        assertTrue(returnType instanceof DrawingBoardDetailedDTO);

    }

    private DrawingBoardDAO getADrawingBoard() {


        return DrawingBoardDAO.builder()
                .id(boardId)
                .backgroundColour("White")
                .height(100)
                .width(100)
                .panHorizontal(0)
                .panVertical(0)
                .zoom(1.0)
                .build();

    }
}