package com.drawingboard.service;

import com.drawingboard.dao.CustomShapeDAO;
import com.drawingboard.dto.AddCustomShapeDTO;
import com.drawingboard.dto.CustomShapeDTO;
import com.drawingboard.dto.DrawingBoardDTO;
import com.drawingboard.dto.ShapeWrapperDTO;
import com.drawingboard.exception.ResourceNotFoundException;
import com.drawingboard.mapper.DrawingBoardMapper;
import com.drawingboard.mapper.DrawingBoardMapperImpl;
import com.drawingboard.model.Circle;
import com.drawingboard.model.Shape;
import com.drawingboard.repository.CustomShapeRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class CustomShapeServiceTest {

    @Mock
    private CustomShapeRepository customShapeRepository;
    @Mock
    private DrawingBoardMapper boardMapper;
    @Mock
    private IShapeService shapeService;
    @Mock
    private IDrawingBoardService boardService;

    private CustomShapeService customShapeService;
    private String customShapeName;
    private int boardId;
    private DrawingBoardMapperImpl mapper;

    @BeforeEach
    void setUp() {
        customShapeService = new CustomShapeService(customShapeRepository, boardMapper);
        customShapeService.setShapeService(shapeService);
        customShapeService.setBoardService(boardService);
        customShapeName = "Custom Shape Name";
        boardId = 3;
        mapper = new DrawingBoardMapperImpl();
    }

    @Test
    void throwErrorWhenAddingAsCustomShapeWithNonExistingDrawingBoard() {
        given(boardService.getById(boardId)).willReturn(null);
        assertThatThrownBy(() -> customShapeService.addCustomShapeWithBoardID(getAddCustomShapeDTO()))
                .isInstanceOf(ResourceNotFoundException.class);
        verify(customShapeRepository, never()).add(any());
    }

    @Test
    void throwErrorIfNoOfShapesAreZeroWhenAddingAsCustomShape() {
        given(boardService.getById(boardId)).willReturn(new DrawingBoardDTO());
        given(shapeService.getShapesByBoardId(boardId)).willReturn(new ArrayList<ShapeWrapperDTO>());
        assertThatThrownBy(() -> customShapeService.addCustomShapeWithBoardID(getAddCustomShapeDTO()))
                .isInstanceOf(RuntimeException.class)
                .hasMessageContaining("here should be atleast 1 shape to add as a customshape");
        verify(customShapeRepository, never()).add(any());
    }

    @Test
    void canAddAsCustomShapeWhenNoError() {
        AddCustomShapeDTO addCustomShapeDTO = getAddCustomShapeDTO();
        ShapeWrapperDTO shapeDTO = ShapeWrapperDTO.builder()
                .shapeName(Shape.CIRCLE.getName())
                .drawingBoardId(boardId)
                .id(1)
                .attributes(new Circle(2))
                .startX(1)
                .startY(1)
                .selected(true)
                .backGroundColor("blue")
                .lineThickness(1)
                .build();
        List<ShapeWrapperDTO> shapeDTOList = new ArrayList<>();
        shapeDTOList.add(shapeDTO);
        CustomShapeDTO customShapeDTO = CustomShapeDTO
                .builder()
                .shapes(mapper.shapewrapperlistDTOtominimalDTO(shapeDTOList))
                .name(customShapeName)
                .build();
        CustomShapeDAO customShapeDAO = mapper.customShapetoDAO(customShapeDTO);
        given(boardService.getById(boardId)).willReturn(new DrawingBoardDTO());
        given(shapeService.getShapesByBoardId(boardId)).willReturn(shapeDTOList);
        given(boardMapper.shapewrapperlistDTOtominimalDTO(shapeDTOList)).willReturn(customShapeDTO.getShapes());
        given(boardMapper.customShapetoDAO(any())).willReturn(customShapeDAO);
        ArgumentCaptor<CustomShapeDAO> argumentCaptor = ArgumentCaptor.forClass(CustomShapeDAO.class);

        customShapeService.addCustomShapeWithBoardID(addCustomShapeDTO);
        verify(customShapeRepository).add(argumentCaptor.capture());
        assertThat(argumentCaptor.getValue().getShapes()).isEqualTo(customShapeDAO.getShapes());
        assertThat(argumentCaptor.getValue().getName()).isEqualTo(customShapeDAO.getName());
    }

    @Test
    void throwsResourceNotFoundExceptionWhenGettingNonExistingShapeByName() {
        given(customShapeRepository.getbyName(customShapeName)).willReturn(null);
        assertThatThrownBy(() -> customShapeService.getShapeByName(customShapeName))
                .isInstanceOf(ResourceNotFoundException.class);
    }

    @Test
    void getCustomShapeWithExistingShapeName() {
        given(customShapeRepository.getbyName(customShapeName)).willReturn(new CustomShapeDAO());
        customShapeService.getShapeByName(customShapeName);
        ArgumentCaptor<String> argumentCaptor = ArgumentCaptor.forClass(String.class);
        verify(customShapeRepository).getbyName(argumentCaptor.capture());
        assertThat(argumentCaptor.getValue()).isEqualTo(customShapeName);
    }

    @Test
    void throwsResourceNotFoundExceptionWhenDeletingNonExistingShapeByName() {
        given(customShapeRepository.getbyName(customShapeName)).willReturn(null);
        assertThatThrownBy(() -> customShapeService.deleteByName(customShapeName))
                .isInstanceOf(ResourceNotFoundException.class);
        verify(customShapeRepository, never()).deleteByName(any());
    }

    @Test
    void canDeleteCustomShapeWithExistingShapeName() {
        given(customShapeRepository.getbyName(customShapeName)).willReturn(new CustomShapeDAO());
        customShapeService.deleteByName(customShapeName);
        ArgumentCaptor<String> argumentCaptor = ArgumentCaptor.forClass(String.class);
        verify(customShapeRepository).deleteByName(argumentCaptor.capture());
        assertThat(argumentCaptor.getValue()).isEqualTo(customShapeName);
    }

    @Test
    void getAll() {
        customShapeService.getAll();
        verify(customShapeRepository).getAll();
    }

    private AddCustomShapeDTO getAddCustomShapeDTO() {
        return AddCustomShapeDTO
                .builder()
                .boardId(boardId)
                .shapeName(customShapeName)
                .build();
    }
}