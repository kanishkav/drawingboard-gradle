package com.drawingboard.service;

import com.drawingboard.dao.ShapeWrapperDAO;
import com.drawingboard.dto.CustomShapeDTO;
import com.drawingboard.dto.DrawingBoardDTO;
import com.drawingboard.dto.ShapeWrapperDTO;
import com.drawingboard.dto.ShapeWrapperMinimalDTO;
import com.drawingboard.exception.ResourceNotFoundException;
import com.drawingboard.mapper.DrawingBoardMapper;
import com.drawingboard.mapper.DrawingBoardMapperImpl;
import com.drawingboard.model.Circle;
import com.drawingboard.model.CustomShape;
import com.drawingboard.model.Shape;
import com.drawingboard.repository.ShapeRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class ShapeServiceTest {
    @Mock
    private ShapeRepository shapeRepository;
    @Mock
    private DrawingBoardMapper drawingBoardMapper;
    @Mock
    private ICustomShapeService customShapeService;
    @Mock
    private IDrawingBoardService boardService;

    private ShapeService shapeService;

    private DrawingBoardMapperImpl mapper;

    private int shapeId;
    private int boardId;
    private String customShapeName;

    @BeforeEach
    void setUp() {
        shapeService = new ShapeService(shapeRepository, drawingBoardMapper);
        shapeService.setBoardService(boardService);
        shapeService.setCustomShapeService(customShapeService);
        shapeId = 4;
        boardId = 3;
        mapper = new DrawingBoardMapperImpl();
        customShapeName = "Custom Shape";
    }

    @Test
    void getAll() {
        shapeService.getAll();
        verify(shapeRepository).getAll();
    }

    @Test
    void throwResourceNotFoundIfNotExists() {
        given(shapeRepository.getbyId(shapeId)).willReturn(null);
        assertThatThrownBy(() -> shapeService.getById(shapeId))
                .isInstanceOf(ResourceNotFoundException.class);
    }

    @Test
    void canGetShapefExists() {
        given(shapeRepository.getbyId(shapeId)).willReturn(new ShapeWrapperDAO());
        ArgumentCaptor<Integer> argumentCaptor = ArgumentCaptor.forClass(Integer.class);
        ShapeWrapperDTO shape = shapeService.getById(shapeId);
        verify(shapeRepository).getbyId(argumentCaptor.capture());
        assertThat(argumentCaptor.getValue()).isEqualTo(shapeId);
    }

    @Test
    void add() {
        ShapeWrapperDTO shape = getAShape();

        ShapeWrapperDAO shapeDAO = drawingBoardMapper.shapeWrapperDTOtoDAO(shape);
        given(shapeRepository.add(shapeDAO)).willReturn(shapeId);
        ArgumentCaptor<ShapeWrapperDAO> argumentCaptor = ArgumentCaptor.forClass(ShapeWrapperDAO.class);
        shapeService.add(shape);
        verify(shapeRepository).add(argumentCaptor.capture());
        assertThat(argumentCaptor.getValue()).isEqualTo(shapeDAO);
        assertThat(shape.getId()).isEqualTo(shapeId);
    }

    @Test
    void throwResourceNotFoundExceptionUpdatingNonExistingShape() {
        ShapeWrapperDTO shape = getAShape();

        given(shapeRepository.getbyId(shapeId)).willReturn(null);
        assertThatThrownBy(() -> shapeService.update(shapeId, shape))
                .isInstanceOf(ResourceNotFoundException.class);
        verify(shapeRepository, never()).update(any());
    }

    @Test
    void throwRuntimeExceptionUpdatingWithIdsNotMatch() {
        ShapeWrapperDTO shape = getAShape();
        shape.setId(3);

        assertThatThrownBy(() -> shapeService.update(shapeId, shape))
                .isInstanceOf(RuntimeException.class)
                .hasMessageContaining("Id s doesnt match");
        verify(shapeRepository, never()).update(any());
    }

    @Test
    void canUpdateWhenShapeExists() {
        ShapeWrapperDTO shape = getAShape();


        ShapeWrapperDAO shapeWrapperDAO = mapper.shapeWrapperDTOtoDAO(shape);
        given(shapeRepository.getbyId(shapeId)).willReturn(shapeWrapperDAO);
        given(drawingBoardMapper.shapeWrapperDAOtoDTO(shapeWrapperDAO)).willReturn(shape);
        given(drawingBoardMapper.shapeWrapperDTOtoDAO(shape)).willReturn(shapeWrapperDAO);
        ArgumentCaptor<ShapeWrapperDAO> argumentCaptor = ArgumentCaptor.forClass(ShapeWrapperDAO.class);

        shapeService.update(shapeId, shape);
        verify(shapeRepository).update(argumentCaptor.capture());
        assertThat(argumentCaptor.getValue()).isEqualTo(shapeWrapperDAO);
    }

    @Test
    void throwResourceNotFoundExceptionWhenDeletingNonExistingShape() {
        given(shapeRepository.getbyId(shapeId)).willReturn(null);
        assertThatThrownBy(() -> shapeService.deleteById(shapeId))
                .isInstanceOf(ResourceNotFoundException.class);
        verify(shapeRepository, never()).deleteById(shapeId);
    }

    @Test
    void canDeleteExistingShape() {
        ShapeWrapperDTO shape = getAShape();
        ShapeWrapperDAO shapeDAO = mapper.shapeWrapperDTOtoDAO(shape);
        given(shapeRepository.getbyId(shapeId)).willReturn(shapeDAO);
        given(drawingBoardMapper.shapeWrapperDAOtoDTO(shapeDAO)).willReturn(shape);

        ArgumentCaptor<Integer> argumentCaptor = ArgumentCaptor.forClass(Integer.class);
        shapeService.deleteById(shapeId);
        verify(shapeRepository).deleteById(argumentCaptor.capture());
        assertThat(argumentCaptor.getValue()).isEqualTo(shapeId);
    }

    @Test
    void thowErrorWhileGettingShapesByNonExistingBoardId() {
        given(boardService.getById(boardId)).willReturn(null);
        assertThatThrownBy(() -> shapeService.getShapesByBoardId(boardId))
                .isInstanceOf(ResourceNotFoundException.class);
        verify(shapeRepository, never()).getShapesByBoardId(boardId);
    }

    @Test
    void canGetShapeListwithExistingBoardId() {
        ShapeWrapperDTO shape = getAShape();
        ShapeWrapperDAO shapeDAO = mapper.shapeWrapperDTOtoDAO(shape);
        List<ShapeWrapperDAO> shapeListDAO = new ArrayList<>();
        shapeListDAO.add(shapeDAO);

        given(boardService.getById(boardId)).willReturn(new DrawingBoardDTO());
        given(drawingBoardMapper.shapeWrapperlistDAOtoDTO(shapeListDAO))
                .willReturn(mapper.shapeWrapperlistDAOtoDTO(shapeListDAO));
        given(shapeRepository.getShapesByBoardId(boardId)).willReturn(shapeListDAO);
        ArgumentCaptor<Integer> argumentCaptor = ArgumentCaptor.forClass(Integer.class);

        Object returnValue = shapeService.getShapesByBoardId(boardId);
        verify(shapeRepository).getShapesByBoardId(argumentCaptor.capture());
        assertThat(argumentCaptor.getValue()).isEqualTo(boardId);
    }

    @Test
    void throwsErrorWhenAddingCustomShapeFromNonExistingDrawingBoardId() {

        ShapeWrapperDTO shape = getAShape();
        given(boardService.getById(boardId)).willReturn(null);
        assertThatThrownBy(() -> shapeService.addCustoms(shape))
                .isInstanceOf(ResourceNotFoundException.class);
        verify(shapeRepository, never()).add(any());
    }

    @Test
    void throwsErrorWhenAddingCustomShapeFromNonExistingDCustomShape() {
        ShapeWrapperDTO shape = getAShape();
        shape.setShapeName(customShapeName);
        given(boardService.getById(boardId)).willReturn(new DrawingBoardDTO());
        given(customShapeService.getShapeByName(customShapeName)).willReturn(null);
        assertThatThrownBy(() -> shapeService.addCustoms(shape))
                .isInstanceOf(ResourceNotFoundException.class);
        verify(shapeRepository, never()).add(any());
    }

    @Test
    void canAddCustomShapeFromExistingDCustomShape() {

        ShapeWrapperDTO shape = getAShape();
        shape.setShapeName(customShapeName);

        DrawingBoardMapperImpl mapper = new DrawingBoardMapperImpl();

        ShapeWrapperDTO shape2 = getAShape();
        shape2.setShapeName(Shape.CUSTOM.getName());
        CustomShape customShape = new CustomShape();
        customShape.setName(customShapeName);
        customShape.setShapes(new ArrayList<ShapeWrapperMinimalDTO>());
        shape2.setAttributes(customShape);

        given(boardService.getById(boardId)).willReturn(new DrawingBoardDTO());
        given(customShapeService.getShapeByName(customShapeName)).willReturn(new CustomShapeDTO());
        given(drawingBoardMapper.shapeWrapperDTOtoDAO(any())).willReturn(mapper.shapeWrapperDTOtoDAO(shape2));
        ArgumentCaptor<ShapeWrapperDAO> argumentCaptor = ArgumentCaptor.forClass(ShapeWrapperDAO.class);
        shapeService.addCustoms(shape);
        verify(shapeRepository).add(argumentCaptor.capture());
        assertThat(argumentCaptor.getValue().getShapeName().getName()).isEqualTo(shape2.getShapeName());
    }

    @Test
    void throwsErrorWhenGettingUndoneShapesByNonExistingBoardId() {
        given(boardService.getById(boardId)).willReturn(null);
        assertThatThrownBy(() -> shapeService.getUndoneShapesByBoardId(boardId))
                .isInstanceOf(ResourceNotFoundException.class);
        verify(shapeRepository, never()).getUndoneShapesByBoardId(boardId);
    }

    @Test
    void canGetUndoneShapesByExistingBoardId() {
        ShapeWrapperDTO shape = getAShape();
        given(boardService.getById(boardId)).willReturn(new DrawingBoardDTO());
        given(shapeRepository.getUndoneShapesByBoardId(boardId)).willReturn(new ArrayList<ShapeWrapperDAO>());
        ArgumentCaptor<Integer> argumentCaptor = ArgumentCaptor.forClass(Integer.class);
        shapeService.getUndoneShapesByBoardId(boardId);
        verify(shapeRepository).getUndoneShapesByBoardId(argumentCaptor.capture());
        assertThat(argumentCaptor.getValue()).isEqualTo(boardId);
    }

    private ShapeWrapperDTO getAShape() {
        return ShapeWrapperDTO.builder()
                .shapeName(Shape.CIRCLE.getName())
                .drawingBoardId(boardId)
                .id(shapeId)
                .attributes(new Circle(2))
                .startX(1)
                .startY(1)
                .selected(true)
                .backGroundColor("blue")
                .lineThickness(1)
                .build();
    }
}