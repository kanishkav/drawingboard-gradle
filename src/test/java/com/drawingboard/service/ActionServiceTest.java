package com.drawingboard.service;

import com.drawingboard.dto.DrawingBoardDTO;
import com.drawingboard.dto.ShapeWrapperDTO;
import com.drawingboard.exception.ResourceNotFoundException;
import com.drawingboard.mapper.DrawingBoardMapper;
import com.drawingboard.mapper.DrawingBoardMapperImpl;
import com.drawingboard.model.Circle;
import com.drawingboard.model.Shape;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class ActionServiceTest {

    @Mock
    private IShapeService shapeService;
    @Mock
    private IDrawingBoardService boardService;
    @Mock
    private DrawingBoardMapper boardMapper;

    private ActionService actionService;
    private DrawingBoardMapperImpl mapper;
    private int boardId;

    @BeforeEach
    void setUp() {
        actionService = new ActionService(shapeService, boardService, boardMapper);
        mapper = new DrawingBoardMapperImpl();
        boardId = 3;
    }

    @Test
    void throwsResourceNotFoundExceptionwithNonExistingBoard() {
        given(boardService.getById(boardId)).willReturn(null);
        assertThatThrownBy(() -> actionService.undo(boardId))
                .isInstanceOf(ResourceNotFoundException.class);
    }

    @Test
    void returnExistingBoardWithoutDoingAnythingWhenShapeListisEmpty() {
        given(boardService.getById(boardId)).willReturn(new DrawingBoardDTO());
        given(shapeService.getShapesByBoardId(boardId)).willReturn(new ArrayList<ShapeWrapperDTO>());
        ArgumentCaptor<Integer> argumentCaptor = ArgumentCaptor.forClass(Integer.class);
        actionService.undo(boardId);
        verify(boardService).getDetailedBoardById(argumentCaptor.capture());
        assertThat(argumentCaptor.getValue()).isEqualTo(boardId);
    }

    @Test
    void undoWhenBoardExistsAndShapeListisNotEmpty() {
        List<ShapeWrapperDTO> shapeDTOList = new ArrayList<>();
        shapeDTOList.add(getAShape());
        given(boardService.getById(boardId)).willReturn(new DrawingBoardDTO());
        given(shapeService.getShapesByBoardId(boardId)).willReturn(shapeDTOList);
        ArgumentCaptor<ShapeWrapperDTO> argumentCaptor = ArgumentCaptor.forClass(ShapeWrapperDTO.class);
        ArgumentCaptor<Integer> argumentCaptor2 = ArgumentCaptor.forClass(Integer.class);
        actionService.undo(boardId);
        verify(shapeService).update(argumentCaptor2.capture(), argumentCaptor.capture());
        assertThat(argumentCaptor.getValue().getSelected()).isEqualTo(false);
        assertThat(argumentCaptor2.getValue()).isEqualTo(shapeDTOList.get(0).getId());
    }

    @Test
    void throwsResourceNotFoundExceptionWhenRedowithNonExistingBoard() {
        given(boardService.getById(boardId)).willReturn(null);
        assertThatThrownBy(() -> actionService.redo(boardId))
                .isInstanceOf(ResourceNotFoundException.class);
    }

    @Test
    void returnExistingBoardWithoutDoingAnythingWithRedoWhenShapeListisEmpty() {
        given(boardService.getById(boardId)).willReturn(new DrawingBoardDTO());
        given(shapeService.getUndoneShapesByBoardId(boardId)).willReturn(new ArrayList<ShapeWrapperDTO>());
        ArgumentCaptor<Integer> argumentCaptor = ArgumentCaptor.forClass(Integer.class);
        actionService.redo(boardId);
        verify(boardService).getDetailedBoardById(argumentCaptor.capture());
        assertThat(argumentCaptor.getValue()).isEqualTo(boardId);
    }

    @Test
    void redoWhenBoardExistsAndShapeListisNotEmpty() {
        List<ShapeWrapperDTO> shapeDTOList = new ArrayList<>();
        ShapeWrapperDTO shapeDTO = getAShape();
        shapeDTO.setSelected(false);
        shapeDTOList.add(shapeDTO);
        given(boardService.getById(boardId)).willReturn(new DrawingBoardDTO());
        given(shapeService.getUndoneShapesByBoardId(boardId)).willReturn(shapeDTOList);
        ArgumentCaptor<ShapeWrapperDTO> argumentCaptor = ArgumentCaptor.forClass(ShapeWrapperDTO.class);
        ArgumentCaptor<Integer> argumentCaptor2 = ArgumentCaptor.forClass(Integer.class);
        actionService.redo(boardId);
        verify(shapeService).update(argumentCaptor2.capture(), argumentCaptor.capture());
        assertThat(argumentCaptor.getValue().getSelected()).isEqualTo(true);
        assertThat(argumentCaptor2.getValue()).isEqualTo(shapeDTOList.get(0).getId());
    }

    private ShapeWrapperDTO getAShape() {
        return ShapeWrapperDTO.builder()
                .shapeName(Shape.CIRCLE.getName())
                .drawingBoardId(boardId)
                .id(1)
                .attributes(new Circle(2))
                .startX(1)
                .startY(1)
                .selected(true)
                .backGroundColor("blue")
                .lineThickness(1)
                .build();
    }

}