package com.drawingboard.mapper;

import com.drawingboard.dao.DrawingBoardDAO;
import com.drawingboard.exception.RepositoryException;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DrawingBoardRowMapper implements RowMapper<DrawingBoardDAO> {
    @Override
    public DrawingBoardDAO mapRow(ResultSet rs, int rowNum) throws SQLException {
        try {
            DrawingBoardDAO board = new DrawingBoardDAO();
            board.setId(rs.getInt("id"));
            board.setHeight(rs.getInt("height"));
            board.setBackgroundColour(rs.getString("backgroundcolor"));
            board.setWidth(rs.getInt("width"));
            board.setPanHorizontal(rs.getDouble("panhorizontal"));
            board.setPanVertical(rs.getDouble("panvertical"));
            board.setZoom(rs.getDouble("zoom"));
            return board;
        } catch (SQLException e) {
            throw new RepositoryException(e);
        }
    }
}
