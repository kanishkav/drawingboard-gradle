package com.drawingboard.mapper;

import com.drawingboard.dao.CustomShapeDAO;
import com.drawingboard.dao.DrawingBoardDAO;
import com.drawingboard.dao.ShapeWrapperDAO;
import com.drawingboard.dto.CustomShapeDTO;
import com.drawingboard.dto.DrawingBoardDTO;
import com.drawingboard.dto.ShapeWrapperDTO;
import com.drawingboard.dto.ShapeWrapperMinimalDTO;
import com.drawingboard.exception.ModelMismatchException;
import com.drawingboard.model.*;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")

public abstract class DrawingBoardMapper {

    // DrawingBoards
    public abstract DrawingBoardDTO boardDAOtoboardDTO(DrawingBoardDAO boardDAO);

    public abstract List<DrawingBoardDTO> boardListDAOtoboardDTO(List<DrawingBoardDAO> boardDAO);

    public abstract DrawingBoardDAO boardDTOtoboardDAO(DrawingBoardDTO boardDTO);

    public abstract List<DrawingBoardDAO> boardListDTOtoboardDAO(List<DrawingBoardDTO> boardDTO);

    public abstract ShapeWrapperMinimalDTO shapewrapperDTOtominimalDTO(ShapeWrapperDTO shapeDTO);

    public abstract List<ShapeWrapperDTO> shapeWrapperlistDAOtoDTO(List<ShapeWrapperDAO> shapeWrapperDAO);

    public abstract List<ShapeWrapperMinimalDTO> shapewrapperlistDTOtominimalDTO(List<ShapeWrapperDTO> shapeDTO);

    public abstract List<ShapeWrapperMinimalDTO> shapewrapperlistDAOtominimalDTO(List<ShapeWrapperDAO> shapeDAOList);

    public abstract List<CustomShapeDTO> customshapeListDAOtoDTO(List<CustomShapeDAO> customShapehapeDAOList);

    //Shapes
    public ShapeWrapperDTO shapeWrapperDAOtoDTO(ShapeWrapperDAO shapeDAO) {
        if (shapeDAO == null) {
            return null;
        }

        ShapeWrapperDTO shapeDTO = new ShapeWrapperDTO();
        shapeDTO.setId(shapeDAO.getId());
        shapeDTO.setDrawingBoardId(shapeDAO.getBoard_id());
        shapeDTO.setShapeName(shapeDAO.getShapeName().getName());
        shapeDTO.setSelected(shapeDAO.getSelected());
        shapeDTO.setBackGroundColor(shapeDAO.getBackGroundColor());
        shapeDTO.setLineThickness(shapeDAO.getLineThickness());
        shapeDTO.setStartX(shapeDAO.getStartX());
        shapeDTO.setStartY(shapeDAO.getStartY());
        shapeDTO.setAttributes(stringToObject(shapeDAO.getAttributes(), shapeDAO.getShapeName()));
        return shapeDTO;
    }

    public ShapeWrapperDAO shapeWrapperDTOtoDAO(ShapeWrapperDTO shapeDTO) {
        if (shapeDTO == null) {
            return null;
        }

        ShapeWrapperDAO shapeDAO = new ShapeWrapperDAO();
        shapeDAO.setId(shapeDTO.getId());
        shapeDAO.setBoard_id(shapeDTO.getDrawingBoardId());
        shapeDAO.setShapeName(Shape.getEnum(shapeDTO.getShapeName()));
        shapeDAO.setSelected(shapeDTO.getSelected());
        shapeDAO.setBackGroundColor(shapeDTO.getBackGroundColor());
        shapeDAO.setLineThickness(shapeDTO.getLineThickness());
        shapeDAO.setStartX(shapeDTO.getStartX());
        shapeDAO.setStartY(shapeDTO.getStartY());
        shapeDAO.setAttributes(objectToString(shapeDTO.getAttributes()));
        if (!shapeDTO.getAttributes().getClass().getSimpleName().equals(shapeDTO.getShapeName())) {
            throw new ModelMismatchException("Attributes of the shape doesnt match the shape " + shapeDAO.getShapeName().getName() + ".");
        }
        return shapeDAO;
    }

    public ShapeWrapperMinimalDTO shapeWrappeMinimalrDAOtoDTO(ShapeWrapperDAO shapeDAO) {
        return shapewrapperDTOtominimalDTO(shapeWrapperDAOtoDTO(shapeDAO));
    }

    // Convert a string of a bashape object to a object
    protected BaseShape stringToObject(String value, Shape shapeName) {
        ObjectMapper oMapper = new ObjectMapper();
        try {
            switch (shapeName) {
                case CIRCLE:
                    Circle circle = oMapper.readValue(value, new TypeReference<Circle>() {
                    });
                    return circle;

                case LINE:
                    Line line = oMapper.readValue(value, new TypeReference<Line>() {
                    });
                    return line;

                case CUSTOM:
                    CustomShape shape = oMapper.readValue(value, new TypeReference<CustomShape>() {
                    });
                    return shape;
            }
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }

    // convert a object to a string
    protected String objectToString(BaseShape shape) {
        ObjectMapper oMapper = new ObjectMapper();
        try {
            String value = oMapper.writeValueAsString(shape);
            return value;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    //custom Shapes
    public CustomShapeDAO customShapetoDAO(CustomShapeDTO customShapeDTO) {
        CustomShapeDAO shapeDAO = new CustomShapeDAO();
        try {
            ObjectMapper mapper = new ObjectMapper();
            shapeDAO.setShapes(mapper.writeValueAsString(customShapeDTO.getShapes()));
            shapeDAO.setName(customShapeDTO.getName());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return shapeDAO;
    }

    public CustomShapeDTO customShapeDAOtoshape(CustomShapeDAO customShapeDAO) {
        CustomShapeDTO shape = new CustomShapeDTO();
        try {
            ObjectMapper mapper = new ObjectMapper();
            shape.setShapes(
                    mapper.readValue(customShapeDAO.getShapes(),
                            new TypeReference<List<ShapeWrapperMinimalDTO>>() {
                            }));
            shape.setName(customShapeDAO.getName());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return shape;
    }
}
