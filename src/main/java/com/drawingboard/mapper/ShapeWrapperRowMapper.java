package com.drawingboard.mapper;

import com.drawingboard.dao.ShapeWrapperDAO;
import com.drawingboard.exception.RepositoryException;
import com.drawingboard.model.Shape;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ShapeWrapperRowMapper implements RowMapper<ShapeWrapperDAO> {
    @Override
    public ShapeWrapperDAO mapRow(ResultSet rs, int rowNum) throws SQLException {
        try {
            ShapeWrapperDAO shape = new ShapeWrapperDAO();
            shape.setId(rs.getInt("id"));
            shape.setBoard_id(rs.getInt("drawingboard_id"));
            shape.setShapeName(Shape.getEnum(rs.getString("shapename")));
            String nn = rs.getString("shapename");
            shape.setSelected(rs.getBoolean("selected"));
            shape.setBackGroundColor(rs.getString("color"));
            shape.setLineThickness(rs.getDouble("linethickness"));
            shape.setStartX(rs.getDouble("startx"));
            shape.setStartY(rs.getDouble("starty"));
            shape.setAttributes(rs.getString("attributes"));
            return shape;
        } catch (SQLException e) {
            throw new RepositoryException(e);
        }
    }
}
