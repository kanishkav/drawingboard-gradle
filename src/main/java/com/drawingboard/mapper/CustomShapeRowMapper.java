package com.drawingboard.mapper;


import com.drawingboard.dao.CustomShapeDAO;
import com.drawingboard.exception.RepositoryException;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CustomShapeRowMapper implements RowMapper<CustomShapeDAO> {

    @Override
    public CustomShapeDAO mapRow(ResultSet rs, int rowNum) throws SQLException {
        try {
            CustomShapeDAO shapeDAO = new CustomShapeDAO();
            shapeDAO.setShapes(rs.getString("shapes"));
            shapeDAO.setName(rs.getString("name"));
            return shapeDAO;
        } catch (SQLException e) {
            throw new RepositoryException(e);
        }
    }
}
