package com.drawingboard.exception;

public class ModelMismatchException extends RuntimeException{
    public ModelMismatchException(String msg){
        super(msg);
    }
}
