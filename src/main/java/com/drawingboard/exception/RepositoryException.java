package com.drawingboard.exception;

import java.sql.SQLException;

public class RepositoryException extends RuntimeException{
    public RepositoryException(Throwable e){
        super(e);
    }
}
