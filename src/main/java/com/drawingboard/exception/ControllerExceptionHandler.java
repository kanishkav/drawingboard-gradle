package com.drawingboard.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

import java.util.Date;

@RestControllerAdvice
public class ControllerExceptionHandler {

    private final Logger logger = LoggerFactory.getLogger(ControllerExceptionHandler.class);

    @ExceptionHandler(value = {ResourceNotFoundException.class})
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public ErrorMessage resourceNotFoundException(ResourceNotFoundException ex, WebRequest request) {
        ErrorMessage errorMessage = new ErrorMessage(
                HttpStatus.NOT_FOUND.value(),
                new Date(),
                ex.getMessage(),
                request.getDescription(false)
        );
        logger.error("Resource Not Found Exception :", ex.getMessage(), ex);
        return errorMessage;
    }

    @ExceptionHandler(value = {ModelMismatchException.class})
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public ErrorMessage modelMismatchException(ModelMismatchException ex, WebRequest request) {
        ErrorMessage errorMessage = new ErrorMessage(
                HttpStatus.BAD_REQUEST.value(),
                new Date(),
                ex.getMessage(),
                request.getDescription(false)
        );
        logger.error("Modelmismatch Exception :", ex.getMessage(), ex);

        return errorMessage;
    }

    @ExceptionHandler(value = {RepositoryException.class})
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public ErrorMessage modelMismatchException(RepositoryException ex, WebRequest request) {
        ErrorMessage errorMessage = new ErrorMessage(
                HttpStatus.INTERNAL_SERVER_ERROR.value(),
                new Date(),
                ex.getMessage(),
                request.getDescription(false)
        );
        logger.error("Repository Exception :", ex.getMessage(), ex);

        return errorMessage;
    }

    @ExceptionHandler(value = {RuntimeException.class})
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorMessage allOtherExceptions(RuntimeException ex, WebRequest request) {
        ErrorMessage errorMessage = new ErrorMessage(
                HttpStatus.INTERNAL_SERVER_ERROR.value(),
                new Date(),
                ex.getMessage(),
                request.getDescription(false)
        );
        logger.error("Undefined Runtime Exception :", ex.getMessage(), ex);

        return errorMessage;
    }

    @ExceptionHandler(value = Exception.class)
    public ResponseEntity<Object> handleThrowable(Exception ex, WebRequest request) {

        logger.error("Undefined Exception :", ex.getMessage(), ex);

        return new ResponseEntity<>(ex.getMessage(), new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
