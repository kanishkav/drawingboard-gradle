package com.drawingboard.repository;

import com.drawingboard.dao.ShapeWrapperDAO;
import com.drawingboard.mapper.ShapeWrapperRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class ShapeRepository extends com.drawingboard.repository.Repository implements IShapeRepository {


    public ShapeRepository(NamedParameterJdbcTemplate template) {
        super(template);
    }

    @Override
    public List<ShapeWrapperDAO> getAll() {
        String query = "SELECT * FROM shapes WHERE selected=true";
        return query(query, new ShapeWrapperRowMapper());

    }

    @Override
    public int add(ShapeWrapperDAO object) {
        try {

            Map<String, Object> params = new HashMap<String, Object>();
            params.put("color", object.getBackGroundColor());
            params.put("linethickness", object.getLineThickness());
            params.put("startx", object.getStartX());
            params.put("starty", object.getStartY());
            params.put("selected", true);
            params.put("shapename", object.getShapeName().getName());
            params.put("attributes", object.getAttributes());
            params.put("drawingboard_id", object.getBoard_id());

            SqlParameterSource paramSource = new MapSqlParameterSource(params);
            KeyHolder key = new GeneratedKeyHolder();
            String query = "INSERT INTO shapes (color, linethickness, startx, starty, selected, shapename, attributes, drawingboard_id) " +
                    "VALUES (:color,:linethickness,:startx,:starty,:selected,:shapename,:attributes,:drawingboard_id)";
            int id = update(query, paramSource, key);
            return key.getKey().intValue();

        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }

    }

    @Override
    public void update(ShapeWrapperDAO object) {

        Map<String, Object> params = new HashMap<String, Object>();
        params.put("attributes", object.getAttributes());
        params.put("shapename", object.getShapeName().getName());
        params.put("starty", object.getStartY());
        params.put("startx", object.getStartX());
        params.put("color", object.getBackGroundColor());
        params.put("selected", object.getSelected());
        params.put("linethickness", object.getLineThickness());
        params.put("drawingboard_id", object.getBoard_id());
        params.put("id", object.getId());

        String query = "UPDATE shapes SET attributes=:attributes,shapename=:shapename,starty=:starty,startx=:startx,color=:color," +
                "selected=:selected,linethickness=:linethickness,drawingboard_id=:drawingboard_id " +
                "WHERE id=:id";
        update(query, params);
    }

    @Override
    public ShapeWrapperDAO getbyId(int id) {
        Map<String, Object> param = new HashMap<>();
        param.put("id", id);
        String query = "SELECT * FROM shapes WHERE id=:id";
        List<ShapeWrapperDAO> shapeDAO = (List<ShapeWrapperDAO>) query(query, param, new ShapeWrapperRowMapper());
        if (shapeDAO.isEmpty()) {
            return null;
        }
        return shapeDAO.get(0);
    }

    @Override
    public void deleteById(int id) {
        Map<String, Object> param = new HashMap<>();
        param.put("id", id);
        String query = "DELETE FROM shapes WHERE id=:id";
        update(query, param);
    }

    @Override
    public List<ShapeWrapperDAO> getShapesByBoardId(int id) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("boardid", id);

        String query = "SELECT * FROM shapes WHERE drawingboard_id=:boardid AND selected=true";
        return query(query, params, new ShapeWrapperRowMapper());
    }

    @Override
    public List<ShapeWrapperDAO> getUndoneShapesByBoardId(int id) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("boardid", id);

        String query = "SELECT * FROM shapes WHERE drawingboard_id=:boardid AND selected=false";
        return query(query, params, new ShapeWrapperRowMapper());
    }


}
