package com.drawingboard.repository;

import com.drawingboard.dao.DrawingBoardDAO;
import com.drawingboard.mapper.DrawingBoardRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class DrawingBoardRepository extends com.drawingboard.repository.Repository implements IDrawingBoardRepository {


    public DrawingBoardRepository(NamedParameterJdbcTemplate template) {
        super(template);
    }

    @Override
    public List<DrawingBoardDAO> getAll() {
        String query = "SELECT * FROM drawingboard";
        return query(query, new DrawingBoardRowMapper());

    }

    @Override
    public int add(DrawingBoardDAO object) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("width", object.getWidth());
        params.put("height", object.getHeight());
        params.put("backgroundcolor", object.getBackgroundColour());
        params.put("zoom", object.getZoom());
        params.put("panvertical", object.getPanVertical());
        params.put("panhorizontal", object.getPanHorizontal());

        SqlParameterSource paramSource = new MapSqlParameterSource(params);
        KeyHolder key = new GeneratedKeyHolder();
        String query = "INSERT INTO drawingboard (width, height, backgroundcolor, zoom, panvertical, panhorizontal) " +
                "VALUES (:width,:height,:backgroundcolor,:zoom,:panvertical,:panhorizontal)";
        int id = this.update(query, paramSource, key);
        return key.getKey().intValue();
    }

    @Override
    public void update(DrawingBoardDAO object) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("width", object.getWidth());
        params.put("height", object.getHeight());
        params.put("backgroundcolor", object.getBackgroundColour());
        params.put("zoom", object.getZoom());
        params.put("panvertical", object.getPanVertical());
        params.put("panhorizontal", object.getPanHorizontal());
        params.put("id", object.getId());

        String query = "UPDATE drawingboard SET width=:width, height=:height, backgroundcolor=:backgroundcolor,zoom=:zoom" +
                ",panhorizontal=:panhorizontal,panvertical=:panvertical WHERE id=:id ";
        int id = update(query, params);
    }

    @Override
    public DrawingBoardDAO getbyId(int id) {
        try {
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("id", id);
            String query = "SELECT * FROM drawingboard WHERE id=:id";
            return query(query, params, new DrawingBoardRowMapper()).get(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void deleteById(int id) {
        Map<String, Object> param = new HashMap<String, Object>();
        param.put("id", id);
        String query = "DELETE FROM drawingboard WHERE id=:id";
        update(query, param);
    }
}
