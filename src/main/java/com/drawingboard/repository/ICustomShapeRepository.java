package com.drawingboard.repository;

import com.drawingboard.dao.CustomShapeDAO;

import java.util.List;

public interface ICustomShapeRepository {
    List<CustomShapeDAO> getAll();

    void add(CustomShapeDAO object);

    void update(CustomShapeDAO object);

    CustomShapeDAO getbyName(String name);

    void deleteByName(String name);
}
