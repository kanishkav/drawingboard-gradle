package com.drawingboard.repository;

import java.util.List;

public interface IRepository<T> {
    List<T> getAll();
    int add(T object);
    void update(T object);
    T getbyId(int id);
    void deleteById(int id);
}
