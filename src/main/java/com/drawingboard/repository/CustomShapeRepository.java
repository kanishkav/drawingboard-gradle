package com.drawingboard.repository;

import com.drawingboard.dao.CustomShapeDAO;
import com.drawingboard.mapper.CustomShapeRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Repository
public class CustomShapeRepository extends com.drawingboard.repository.Repository implements ICustomShapeRepository {

    public CustomShapeRepository(NamedParameterJdbcTemplate template) {
        super(template);
    }

    @Override
    public List<CustomShapeDAO> getAll() {
        String query = "SELECT * FROM customshapes";
        return query(query, new CustomShapeRowMapper());
    }

    @Override
    public void add(CustomShapeDAO object) {
        Map<String, Object> params = new HashMap<>();
        params.put("name", object.getName());
        params.put("shapes", object.getShapes());

        String query = "INSERT INTO customshapes (name, shapes) VALUES (:name,:shapes)";
        int rows = update(query, params);
    }

    @Override
    public void update(CustomShapeDAO object) {

    }

    @Override
    public CustomShapeDAO getbyName(String name) {
        Map<String, Object> params = new HashMap<>();
        params.put("name", name);
        String query = "SELECT * FROM customshapes WHERE name=:name";
        List<CustomShapeDAO> shapesDAO = query(query, params, new CustomShapeRowMapper());
        if (shapesDAO.isEmpty()) {
            return null;
        }
        return shapesDAO.get(0);
    }

    @Override
    public void deleteByName(String name) {
        Map<String, Object> params = new HashMap<>();
        params.put("name", name);
        String query = "DELETE FROM customshapes WHERE name=:name";
        update(query, params);
    }

}
