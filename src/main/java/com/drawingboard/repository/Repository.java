package com.drawingboard.repository;

import com.drawingboard.exception.RepositoryException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.KeyHolder;

import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

@Slf4j
public abstract class Repository {
    private final NamedParameterJdbcTemplate template;

    public Repository(NamedParameterJdbcTemplate template) {
        this.template = template;
    }

    protected <T> List<T> query(String query, RowMapper<T> mapper) {
        log.trace(query);
        return this.errorProcessing(()->{
            return template.query( query, mapper);
        });
    }

    protected <T> List<T> query(String query, Map<String, Object> params, RowMapper<T> mapper) {
        log.trace(query);
        return this.errorProcessing(()->{
            return template.query(query,params, mapper);
        });
    }

    protected int update(String query, SqlParameterSource params, KeyHolder keyHolder) {
        log.trace(query);
        return this.errorProcessing(()->{
           return template.update(query, params, keyHolder);
        });
    }

    protected int update(String query, Map<String, Object> params) {
        log.trace(query);
        return this.errorProcessing(()->{
            return template.update(query, params);
        });
    }

    protected <T> T errorProcessing(Callable<T> toExec) throws RepositoryException {
        try {
            return toExec.call();
        } catch (Throwable e) {
            if (e instanceof RepositoryException) {
                throw (RepositoryException) e;
            }
            throw new RepositoryException(e);
        }
    }

}
