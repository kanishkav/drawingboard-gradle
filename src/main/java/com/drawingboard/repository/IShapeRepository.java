package com.drawingboard.repository;


import com.drawingboard.dao.ShapeWrapperDAO;

import java.util.List;

public interface IShapeRepository extends IRepository<ShapeWrapperDAO> {

    List<ShapeWrapperDAO> getShapesByBoardId(int id);

    List<ShapeWrapperDAO> getUndoneShapesByBoardId(int id);
}
