package com.drawingboard.repository;

import com.drawingboard.dao.DrawingBoardDAO;

public interface IDrawingBoardRepository extends IRepository<DrawingBoardDAO> {
}
