package com.drawingboard.dto;

import com.drawingboard.model.BaseShape;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ShapeWrapperDTO {

	// Fields
	private int id;

	private String backGroundColor;

	private double lineThickness;

	private double startX;

	private double startY;

	private Boolean selected;

	private String shapeName;

	private BaseShape attributes;

	private int drawingBoardId;

}
