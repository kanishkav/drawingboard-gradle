package com.drawingboard.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class DrawingBoardDTO {

    // Fields

    private int id;

    private int width;

    private int height;

    private String backgroundColour = "White";

    private double zoom = 1;

    private double panVertical = 0;

    private double panHorizontal = 0;
}
