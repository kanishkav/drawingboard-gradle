package com.drawingboard.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@NoArgsConstructor
@Getter
@Setter
public class DrawingBoardDetailedDTO {
    DrawingBoardDTO board;
    List<ShapeWrapperMinimalDTO> shapes;
}
