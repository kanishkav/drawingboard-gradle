package com.drawingboard.dto;

import com.drawingboard.model.BaseShape;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class ShapeWrapperMinimalDTO {
    // Fields
    private int id;

    private String backGroundColor;

    private double lineThickness;

    private double startX;

    private double startY;

    private String shapeName;

    private BaseShape attributes;
}
