package com.drawingboard.dto;

import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CustomShapeDTO {

    private String name;
    private List<ShapeWrapperMinimalDTO> shapes;
}
