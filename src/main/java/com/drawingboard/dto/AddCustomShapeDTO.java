package com.drawingboard.dto;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AddCustomShapeDTO {
    private int boardId;
    private String shapeName;
}
