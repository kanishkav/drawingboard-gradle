package com.drawingboard;

import com.drawingboard.dao.ShapeWrapperDAO;
import com.drawingboard.model.Line;
import com.drawingboard.model.Point;
import com.drawingboard.model.Shape;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DrawingboardApplication {


    public static void main(String[] args) {
        SpringApplication.run(DrawingboardApplication.class, args);

    }

}
