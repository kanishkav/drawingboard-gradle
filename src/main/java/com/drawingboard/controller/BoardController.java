package com.drawingboard.controller;

import com.drawingboard.dto.AddCustomShapeDTO;
import com.drawingboard.dto.CustomShapeDTO;
import com.drawingboard.dto.DrawingBoardDTO;
import com.drawingboard.dto.DrawingBoardDetailedDTO;
import com.drawingboard.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/")
public class BoardController {

    private final IDrawingBoardService drawingBoardService;
    private final ICustomShapeService customShapeService;
    private IZoomService zoomService;
    private IPanService panService;
    private IActionService actionService;

    public BoardController(IDrawingBoardService drawingBoardService, ICustomShapeService customShapeService) {
        this.drawingBoardService = drawingBoardService;
        this.customShapeService = customShapeService;
    }

    @Autowired
    public void setZoomService(IZoomService zoomService) {
        this.zoomService = zoomService;
    }

    @Autowired
    public void setActionService(IActionService actionService) {
        this.actionService = actionService;
    }

    @Autowired
    public void setPanService(IPanService panService) {
        this.panService = panService;
    }

    @GetMapping("boards")
    public List<DrawingBoardDTO> getAll() {
        return drawingBoardService.getAll();
    }

    @GetMapping("boards/{id}")
    public DrawingBoardDTO getById(@PathVariable int id) {
        return drawingBoardService.getById(id);
    }

    @PostMapping("boards")
    public DrawingBoardDTO add(@RequestBody DrawingBoardDTO boardDTO) {
        drawingBoardService.add(boardDTO);
        return boardDTO;
    }

    @PutMapping("boards/{id}")
    public void update(@RequestBody DrawingBoardDTO boardDTO, @PathVariable int id) {
        drawingBoardService.update(boardDTO, id);
    }

    @DeleteMapping("boards/{id}")
    public DrawingBoardDTO delete(@PathVariable int id) {
        return drawingBoardService.deleteById(id);
    }


    @GetMapping("boards/{boardId}/detailed")
    public DrawingBoardDetailedDTO getBoardByIdDetailed(@PathVariable int boardId) {
        return drawingBoardService.getDetailedBoardById(boardId);
    }

    @PostMapping("boards/{boardid}/saveascustom")
    public CustomShapeDTO addBoardasCustomShape(@RequestBody AddCustomShapeDTO shape) {
        return customShapeService.addCustomShapeWithBoardID(shape);

    }

    @GetMapping("boards/{id}/zoom/{zoomLevel}")
    public DrawingBoardDetailedDTO zoomDrawingBaord(@PathVariable double zoomLevel, @PathVariable int id) {
        return zoomService.zoomBoard(id, zoomLevel);
    }

    @GetMapping("boards/{id}/pan")
    public DrawingBoardDetailedDTO panDrawingBoard(
            @PathVariable int id,
            @RequestParam("x") double panX,
            @RequestParam("y") double panY) {
        return panService.pan(id, panX, panY);
    }

    @GetMapping("boards/{id}/undo")
    public DrawingBoardDetailedDTO undo(@PathVariable int id) {
        return actionService.undo(id);

    }

    @GetMapping("boards/{id}/redo")
    public DrawingBoardDetailedDTO redo(@PathVariable int id) {
        return actionService.redo(id);
    }
}