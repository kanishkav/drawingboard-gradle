package com.drawingboard.controller;

import com.drawingboard.dto.CustomShapeDTO;
import com.drawingboard.dto.ShapeWrapperDTO;
import com.drawingboard.service.CustomShapeService;
import com.drawingboard.service.IShapeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/")
public class ShapeController {

    private final IShapeService shapeService;
    private CustomShapeService customShapeService;

    @Autowired
    public ShapeController(IShapeService shapeService) {
        this.shapeService = shapeService;
    }

    @Autowired
    public void setCustomShapeService(CustomShapeService customShapeService) {
        this.customShapeService = customShapeService;
    }

    @GetMapping("shapes")
    public List<ShapeWrapperDTO> getAll() {
        return shapeService.getAll();
    }

    @PostMapping("shapes")
    public ShapeWrapperDTO addShape(@RequestBody ShapeWrapperDTO shapeDTO) {
        shapeService.add(shapeDTO);
        return shapeDTO;
    }

    @PostMapping("shapes/addcustom")
    public ShapeWrapperDTO addCustomShape(@RequestBody ShapeWrapperDTO shapeDTO) {
        shapeService.addCustoms(shapeDTO);
        return shapeDTO;
    }

    @PutMapping("shapes/{id}")
    public void editShape(@RequestBody ShapeWrapperDTO shapeDTO, @PathVariable int id) {
        shapeService.update(id, shapeDTO);
    }

    @DeleteMapping("shapes/{id}")
    public ShapeWrapperDTO delete(@PathVariable int id) {

        return shapeService.deleteById(id);
    }

    @DeleteMapping("shapes/{name}/custom")
    public CustomShapeDTO delete(@PathVariable String name) {
        return customShapeService.deleteByName(name);
    }

    @GetMapping("shapes/custom")
    public List<CustomShapeDTO> getAllCustomShapes() {
        return customShapeService.getAll();
    }
}
