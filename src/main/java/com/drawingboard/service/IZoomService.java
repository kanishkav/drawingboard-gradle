package com.drawingboard.service;

import com.drawingboard.dto.DrawingBoardDetailedDTO;

public interface IZoomService {
    DrawingBoardDetailedDTO zoomBoard(int id, double zoomLevel);
}
