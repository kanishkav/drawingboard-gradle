package com.drawingboard.service;

import com.drawingboard.dto.DrawingBoardDetailedDTO;
import com.drawingboard.dto.ShapeWrapperMinimalDTO;
import com.drawingboard.exception.ModelMismatchException;
import com.drawingboard.exception.ResourceNotFoundException;
import com.drawingboard.model.*;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ZoomService implements IZoomService {

    private final IDrawingBoardService drawingBoardService;
    private final IShapeService shapeService;

    public ZoomService(IDrawingBoardService drawingBoardService, IShapeService shapeService) {
        this.drawingBoardService = drawingBoardService;
        this.shapeService = shapeService;
    }

    @Override
    public DrawingBoardDetailedDTO zoomBoard(int id, double zoomLevel) {
        DrawingBoardDetailedDTO boardDTO = Optional.ofNullable(drawingBoardService.getDetailedBoardById(id))
                .orElseThrow(() -> new ResourceNotFoundException("Board with id " + id + " Not found"));

        Point mid = new Point();
        mid.setX(boardDTO.getBoard().getWidth() / 2);
        mid.setY(boardDTO.getBoard().getHeight() / 2);

        boardDTO.setShapes(zoomShapes(boardDTO.getShapes(), mid, zoomLevel, boardDTO.getBoard().getZoom()));
        boardDTO.getBoard().setZoom(zoomLevel);
        return boardDTO;
    }


    private List<ShapeWrapperMinimalDTO> zoomShapes(
            List<ShapeWrapperMinimalDTO> shapeDTOList,
            Point mid,
            double zoomLevel,
            double currentZoom) {

        for (ShapeWrapperMinimalDTO shape : shapeDTOList) {
            shape.setLineThickness(shape.getLineThickness() * zoomLevel / currentZoom);
            shape.setStartX(zoomPoint(shape.getStartX(), zoomLevel, currentZoom, mid.getX()));
            shape.setStartY(zoomPoint(shape.getStartY(), zoomLevel, currentZoom, mid.getY()));
            shape.setAttributes(zoomAttributes(shape.getAttributes(), shape.getShapeName(), zoomLevel, currentZoom, mid));
        }
        return shapeDTOList;

    }


    private BaseShape zoomAttributes(BaseShape attributes, String shapeName, double zoomLevel, double currentZoom, Point mid) {

        switch (Shape.getEnum(shapeName)) {
            case CUSTOM:
                CustomShape customShape = (CustomShape) attributes;
                customShape.setShapes(zoomShapes(customShape.getShapes(), mid, zoomLevel, currentZoom));
                return customShape;
            case CIRCLE:
                Circle circle = (Circle) attributes;
                circle.setRadius(circle.getRadius() * zoomLevel / currentZoom);
                return circle;

            case LINE:
                Line line = (Line) attributes;
                Point point = line.getEndPoint();
                point.setX(zoomPoint(point.getX(), zoomLevel, currentZoom, mid.getX()));
                point.setY(zoomPoint(point.getY(), zoomLevel, currentZoom, mid.getY()));
                line.setEndPoint(point);
                return line;
        }
        throw new ModelMismatchException("Shapes doesnt match");
    }


    private double zoomPoint(double point, double zoomLevel, double currentZoom, double mid) {

        return ((point - mid) * zoomLevel / currentZoom) + mid;
    }


}
