package com.drawingboard.service;

import com.drawingboard.dao.DrawingBoardDAO;
import com.drawingboard.dto.DrawingBoardDTO;
import com.drawingboard.dto.DrawingBoardDetailedDTO;
import com.drawingboard.dto.ShapeWrapperMinimalDTO;
import com.drawingboard.exception.ResourceNotFoundException;
import com.drawingboard.mapper.DrawingBoardMapper;
import com.drawingboard.repository.IDrawingBoardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DrawingBoardService implements IDrawingBoardService {

    private final IDrawingBoardRepository drawingBoardRepository;

    private final DrawingBoardMapper drawingBoardMapper;

    private IShapeService shapeService;

    public DrawingBoardService(IDrawingBoardRepository drawingBoardRepository, DrawingBoardMapper drawingBoardMapper) {
        this.drawingBoardRepository = drawingBoardRepository;
        this.drawingBoardMapper = drawingBoardMapper;
    }

    @Autowired
    public void setShapeService(IShapeService shapeService) {
        this.shapeService = shapeService;
    }

    @Override
    public List<DrawingBoardDTO> getAll() {
        return drawingBoardMapper.boardListDAOtoboardDTO(drawingBoardRepository.getAll());
    }

    @Override
    public DrawingBoardDTO getById(int id) {
        DrawingBoardDAO boardDAO = Optional.ofNullable(drawingBoardRepository.getbyId(id))
                .orElseThrow(() -> new ResourceNotFoundException("Board by Id " + id + " Not Found."));
        return drawingBoardMapper.boardDAOtoboardDTO(boardDAO);
    }

    @Override
    public void add(DrawingBoardDTO object) {
        DrawingBoardDAO boardDAO = drawingBoardMapper.boardDTOtoboardDAO(object);
        int boardId = drawingBoardRepository.add(boardDAO);
        object.setId(boardId);

    }

    @Override
    public void update(DrawingBoardDTO object, int id) throws RuntimeException {
        if (object == null || object.getId() != id) {
            throw new RuntimeException("Bad Request id doesnt match");
        }
        DrawingBoardDAO boardDAO = Optional.ofNullable(drawingBoardRepository.getbyId(object.getId()))
                .orElseThrow(() -> new ResourceNotFoundException("Drawing board with id " + id + " Not Found."));

        boardDAO.setWidth(object.getWidth());
        boardDAO.setZoom(object.getZoom());
        boardDAO.setHeight(object.getHeight());
        boardDAO.setBackgroundColour(object.getBackgroundColour());
        boardDAO.setPanHorizontal(object.getPanHorizontal());
        boardDAO.setPanVertical(object.getPanVertical());
        drawingBoardRepository.update(boardDAO);
    }

    @Override
    public DrawingBoardDTO deleteById(int id) {
        DrawingBoardDAO boardDAO = Optional.ofNullable(drawingBoardRepository.getbyId(id))
                .orElseThrow(() -> new ResourceNotFoundException("Drawing board with id " + id + " Not Found."));

        drawingBoardRepository.deleteById(id);

        return drawingBoardMapper.boardDAOtoboardDTO(boardDAO);
    }

    @Override
    public DrawingBoardDetailedDTO getDetailedBoardById(int id) {
        DrawingBoardDAO boardDAO = Optional.ofNullable(drawingBoardRepository.getbyId(id))
                .orElseThrow(() -> new ResourceNotFoundException("Drawing board with id " + id + " Not Found."));
        DrawingBoardDetailedDTO boardDetailedDTO = new DrawingBoardDetailedDTO();

        List<ShapeWrapperMinimalDTO> shapesDTO =
                drawingBoardMapper.shapewrapperlistDTOtominimalDTO(shapeService.getShapesByBoardId(id));
        boardDetailedDTO.setShapes(shapesDTO);
        boardDetailedDTO.setBoard(drawingBoardMapper.boardDAOtoboardDTO(boardDAO));
        return boardDetailedDTO;
    }
}
