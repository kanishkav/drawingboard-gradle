package com.drawingboard.service;

import com.drawingboard.dto.ShapeWrapperDTO;
import com.drawingboard.exception.ResourceNotFoundException;

import java.util.List;

public interface IShapeService {

    List<ShapeWrapperDTO> getAll();

    ShapeWrapperDTO getById(int id) throws ResourceNotFoundException;

    void add(ShapeWrapperDTO object);

    void update(int id, ShapeWrapperDTO object) throws ResourceNotFoundException;

    ShapeWrapperDTO deleteById(int id) throws ResourceNotFoundException;

    List<ShapeWrapperDTO> getShapesByBoardId(int boardId);

    void addCustoms(ShapeWrapperDTO shapeDTO);

    List<ShapeWrapperDTO> getUndoneShapesByBoardId(int id) throws ResourceNotFoundException;
}
