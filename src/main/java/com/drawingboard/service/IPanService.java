package com.drawingboard.service;

import com.drawingboard.dto.DrawingBoardDetailedDTO;

public interface IPanService {
    DrawingBoardDetailedDTO pan(int id, double panX, double panY);
}
