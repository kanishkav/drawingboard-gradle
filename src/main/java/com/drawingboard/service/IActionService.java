package com.drawingboard.service;

import com.drawingboard.dto.DrawingBoardDetailedDTO;

public interface IActionService {
    DrawingBoardDetailedDTO undo(int id);

    DrawingBoardDetailedDTO redo(int id);
}
