package com.drawingboard.service;

import com.drawingboard.dto.DrawingBoardDTO;
import com.drawingboard.dto.DrawingBoardDetailedDTO;
import com.drawingboard.dto.ShapeWrapperDTO;
import com.drawingboard.exception.ResourceNotFoundException;
import com.drawingboard.mapper.DrawingBoardMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ActionService implements IActionService {

    private final IShapeService shapeService;
    private final IDrawingBoardService boardService;
    private final DrawingBoardMapper mapper;

    public ActionService(IShapeService shapeService, IDrawingBoardService boardService, DrawingBoardMapper mapper) {
        this.shapeService = shapeService;
        this.boardService = boardService;
        this.mapper = mapper;
    }

    @Override
    public DrawingBoardDetailedDTO undo(int id) {
        DrawingBoardDTO board = Optional.ofNullable(boardService.getById(id))
                .orElseThrow(() -> new ResourceNotFoundException("Board with id " + id + " Not found"));

        List<ShapeWrapperDTO> shapes = shapeService.getShapesByBoardId(id);
        if (shapes.isEmpty()) {
            return boardService.getDetailedBoardById(id);
        }
        ShapeWrapperDTO shape = shapes.get(shapes.size() - 1);
        shape.setSelected(false);
        shapeService.update(shape.getId(), shape);

        return boardService.getDetailedBoardById(id);
    }

    @Override
    public DrawingBoardDetailedDTO redo(int id) {
        DrawingBoardDTO board = Optional.ofNullable(boardService.getById(id))
                .orElseThrow(() -> new ResourceNotFoundException("Board with id " + id + " Not found"));

        List<ShapeWrapperDTO> shapes = shapeService.getUndoneShapesByBoardId(id);
        if (shapes.isEmpty()) {
            return boardService.getDetailedBoardById(id);
        }
        ShapeWrapperDTO shape = shapes.get(0);
        shape.setSelected(true);
        shapeService.update(shape.getId(), shape);

        return boardService.getDetailedBoardById(id);

    }
}
