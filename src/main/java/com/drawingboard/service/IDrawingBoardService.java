package com.drawingboard.service;

import com.drawingboard.dto.DrawingBoardDTO;
import com.drawingboard.dto.DrawingBoardDetailedDTO;
import com.drawingboard.exception.ResourceNotFoundException;

import java.util.List;

public interface IDrawingBoardService {
    List<DrawingBoardDTO> getAll();

    DrawingBoardDTO getById(int id) throws ResourceNotFoundException;

    void add(DrawingBoardDTO object);

    void update(DrawingBoardDTO object, int id) throws RuntimeException;

    DrawingBoardDTO deleteById(int id) throws ResourceNotFoundException;

    DrawingBoardDetailedDTO getDetailedBoardById(int id) throws ResourceNotFoundException;
}
