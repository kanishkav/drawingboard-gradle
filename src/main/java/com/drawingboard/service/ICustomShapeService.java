package com.drawingboard.service;

import com.drawingboard.dto.AddCustomShapeDTO;
import com.drawingboard.dto.CustomShapeDTO;
import com.drawingboard.exception.ResourceNotFoundException;
import com.drawingboard.model.CustomShape;

import java.util.List;

public interface ICustomShapeService {
    public CustomShapeDTO addCustomShapeWithBoardID(AddCustomShapeDTO addCustomshapeDTO);
    public CustomShapeDTO getShapeByName(String name);

    CustomShapeDTO deleteByName(String name) throws ResourceNotFoundException;

    List<CustomShapeDTO> getAll();
}
