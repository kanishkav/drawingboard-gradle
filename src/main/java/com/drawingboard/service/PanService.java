package com.drawingboard.service;

import com.drawingboard.dto.DrawingBoardDetailedDTO;
import com.drawingboard.dto.ShapeWrapperMinimalDTO;
import com.drawingboard.exception.ResourceNotFoundException;
import com.drawingboard.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PanService implements IPanService {

    private final IDrawingBoardService drawingBoardService;

    @Autowired
    public PanService(IDrawingBoardService drawingBoardService) {
        this.drawingBoardService = drawingBoardService;
    }

    @Override
    public DrawingBoardDetailedDTO pan(int id, double panX, double panY) {
        DrawingBoardDetailedDTO boardDTO = Optional.ofNullable(drawingBoardService.getDetailedBoardById(id))
                .orElseThrow(() -> new ResourceNotFoundException("Board with id " + id + " Not found"));
        if (boardDTO == null) {
            throw new RuntimeException("Not Found Board");
        }
        boardDTO.setShapes(panShapes(boardDTO.getShapes(), panX, panY));
        boardDTO.getBoard().setPanHorizontal(panX);
        boardDTO.getBoard().setPanVertical(panY);
        return boardDTO;
    }

    private List<ShapeWrapperMinimalDTO> panShapes(List<ShapeWrapperMinimalDTO> shapeList, double panX, double panY) {
        for (ShapeWrapperMinimalDTO shape : shapeList) {
            shape.setStartY(shape.getStartY() + panY);
            shape.setStartX(shape.getStartX() + panX);
            shape.setAttributes(panAttributes(shape.getAttributes(), shape.getShapeName(), panX, panY));
        }
        return shapeList;
    }

    private BaseShape panAttributes(BaseShape attributes, String shapeName, double panX, double panY) {

        switch (Shape.getEnum(shapeName)) {
            case CUSTOM:
                CustomShape customShape = (CustomShape) attributes;
                customShape.setShapes(panShapes(customShape.getShapes(), panX, panY));
                return customShape;
            case CIRCLE:
                return attributes;

            case LINE:
                Line line = (Line) attributes;
                Point point = line.getEndPoint();
                point.setX(point.getX() + panX);
                point.setY(point.getY() + panY);
                line.setEndPoint(point);
                return line;
        }
        throw new ResourceNotFoundException("Shape Didn't find with name" + shapeName);
    }
}
