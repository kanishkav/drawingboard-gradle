package com.drawingboard.service;

import com.drawingboard.dao.ShapeWrapperDAO;
import com.drawingboard.dto.CustomShapeDTO;
import com.drawingboard.dto.DrawingBoardDTO;
import com.drawingboard.dto.ShapeWrapperDTO;
import com.drawingboard.exception.ModelMismatchException;
import com.drawingboard.exception.ResourceNotFoundException;
import com.drawingboard.mapper.DrawingBoardMapper;
import com.drawingboard.model.CustomShape;
import com.drawingboard.repository.IShapeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ShapeService implements IShapeService {

    private final IShapeRepository shapeRepository;
    private final DrawingBoardMapper drawingBoardMapper;
    private ICustomShapeService customShapeService;
    private IDrawingBoardService boardService;

    @Autowired
    public ShapeService(
            IShapeRepository shapeRepository, DrawingBoardMapper drawingBoardMapper) {
        this.shapeRepository = shapeRepository;
        this.drawingBoardMapper = drawingBoardMapper;
    }

    @Autowired
    public void setCustomShapeService(ICustomShapeService customShapeService) {
        this.customShapeService = customShapeService;
    }

    @Autowired
    public void setBoardService(IDrawingBoardService boardService) {
        this.boardService = boardService;
    }

    @Override
    public List<ShapeWrapperDTO> getAll() {
        return drawingBoardMapper.shapeWrapperlistDAOtoDTO(shapeRepository.getAll());
    }

    @Override
    public ShapeWrapperDTO getById(int id) {
        ShapeWrapperDAO shapeDAO = Optional.ofNullable(shapeRepository.getbyId(id))
                .orElseThrow(() -> new ResourceNotFoundException("Shape with id " + id + " Not found"));
        return drawingBoardMapper.shapeWrapperDAOtoDTO(shapeDAO);
    }

    @Override
    public void add(ShapeWrapperDTO object) {
        try {
            ShapeWrapperDAO shapeDAO = drawingBoardMapper.shapeWrapperDTOtoDAO(object);
            int shapeId = shapeRepository.add(shapeDAO);

            List<ShapeWrapperDAO> shapes = shapeRepository.getUndoneShapesByBoardId(object.getDrawingBoardId());
            for (ShapeWrapperDAO shape : shapes) {
                shapeRepository.deleteById(shape.getId());
            }
            object.setId(shapeId);
        } catch (ModelMismatchException e) {
            throw e;
        }

    }

    @Override
    public void update(int id, ShapeWrapperDTO object) {
        if (object == null || id != object.getId()) {
            throw new RuntimeException("Id s doesnt match");
        }
        ShapeWrapperDAO shapeDAO = Optional.ofNullable(shapeRepository.getbyId(id))
                .orElseThrow(() -> new ResourceNotFoundException("Shape with id " + id + " Not found"));
        ShapeWrapperDTO shapeinDB = drawingBoardMapper.shapeWrapperDAOtoDTO(shapeDAO);

        shapeinDB.setShapeName(object.getShapeName());
        shapeinDB.setAttributes(object.getAttributes());
        shapeinDB.setBackGroundColor(object.getBackGroundColor());
        shapeinDB.setLineThickness(object.getLineThickness());
        shapeinDB.setStartX(object.getStartX());
        shapeinDB.setStartY(object.getStartY());
        shapeinDB.setDrawingBoardId(object.getDrawingBoardId());
        shapeinDB.setSelected(object.getSelected());

        shapeRepository.update(drawingBoardMapper.shapeWrapperDTOtoDAO(shapeinDB));
    }

    @Override
    public ShapeWrapperDTO deleteById(int id) {
        ShapeWrapperDAO shapeDAO = Optional.ofNullable(shapeRepository.getbyId(id))
                .orElseThrow(() -> new ResourceNotFoundException("Shape with id " + id + " Not found"));

        shapeRepository.deleteById(id);
        return drawingBoardMapper.shapeWrapperDAOtoDTO(shapeDAO);
    }

    @Override
    public List<ShapeWrapperDTO> getShapesByBoardId(int boardId) {
        Optional.ofNullable(boardService.getById(boardId))
                .orElseThrow(() -> new ResourceNotFoundException("Board with id " + boardId + " Not found"));
        return drawingBoardMapper.shapeWrapperlistDAOtoDTO(shapeRepository.getShapesByBoardId(boardId));

    }

    @Override
    public void addCustoms(ShapeWrapperDTO shapeDTO) {
        Optional.ofNullable(boardService.getById(shapeDTO.getDrawingBoardId()))
                .orElseThrow(() -> new ResourceNotFoundException("Board with id " + shapeDTO.getDrawingBoardId() + " Not found"));
        CustomShapeDTO customShapeDTO = customShapeService.getShapeByName(shapeDTO.getShapeName());
        shapeDTO.setShapeName("Custom");
        if (customShapeDTO == null) {
            throw new ResourceNotFoundException("No Custom Shape found by the name");
        }
        CustomShape customShape = new CustomShape();
        customShape.setShapes(customShapeDTO.getShapes());
        customShape.setName(customShapeDTO.getName());

        shapeDTO.setAttributes(customShape);
        ShapeWrapperDAO shapeDAO = drawingBoardMapper.shapeWrapperDTOtoDAO(shapeDTO);
        int id = shapeRepository.add(shapeDAO);
        shapeDTO.setId(id);
    }

    @Override
    public List<ShapeWrapperDTO> getUndoneShapesByBoardId(int id) {
        DrawingBoardDTO board = Optional.ofNullable(boardService.getById(id))
                .orElseThrow(() -> new ResourceNotFoundException("Board with id " + id + " Not found"));
        List<ShapeWrapperDAO> shapeDAOs = shapeRepository.getUndoneShapesByBoardId(id);

        if (shapeDAOs.isEmpty()) {
            return new ArrayList<ShapeWrapperDTO>();
        }
        return drawingBoardMapper.shapeWrapperlistDAOtoDTO(shapeDAOs);
    }
}
