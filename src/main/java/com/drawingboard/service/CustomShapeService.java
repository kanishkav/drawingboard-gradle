package com.drawingboard.service;

import com.drawingboard.dao.CustomShapeDAO;
import com.drawingboard.dto.*;
import com.drawingboard.exception.ResourceNotFoundException;
import com.drawingboard.mapper.DrawingBoardMapper;
import com.drawingboard.repository.ICustomShapeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CustomShapeService implements ICustomShapeService {

    private final ICustomShapeRepository customShapeRepository;
    private IShapeService shapeService;
    private IDrawingBoardService boardService;
    private final DrawingBoardMapper boardMapper;

    public CustomShapeService(
            ICustomShapeRepository customShapeRepository,
            DrawingBoardMapper boardMapper) {
        this.customShapeRepository = customShapeRepository;
        this.boardMapper = boardMapper;
    }

    @Autowired
    public void setBoardService(IDrawingBoardService boardService) {
        this.boardService = boardService;
    }

    @Autowired
    public void setShapeService(IShapeService shapeService) {
        this.shapeService = shapeService;
    }

    public CustomShapeDTO addCustomShapeWithBoardID(AddCustomShapeDTO addCustomshapeDTO) {
        DrawingBoardDTO board =Optional.ofNullable(boardService.getById(addCustomshapeDTO.getBoardId()))
                .orElseThrow(()->new ResourceNotFoundException("No Drawing Board Id found with "+addCustomshapeDTO.getBoardId()));

        List<ShapeWrapperDTO> shapes = shapeService.getShapesByBoardId(addCustomshapeDTO.getBoardId());
        if (shapes.isEmpty()) {
            throw new RuntimeException("There should be atleast 1 shape to add as a customshape");
        }

        CustomShapeDTO customShape = new CustomShapeDTO();
        customShape.setName(addCustomshapeDTO.getShapeName());
        List<ShapeWrapperMinimalDTO> shapelist = boardMapper.shapewrapperlistDTOtominimalDTO(shapes);
        customShape.setShapes(shapelist);

        customShapeRepository.add(boardMapper.customShapetoDAO(customShape));
        return customShape;
    }

    @Override
    public CustomShapeDTO getShapeByName(String name) {
        CustomShapeDAO shapeDAO = Optional.ofNullable(customShapeRepository.getbyName(name))
                .orElseThrow(() -> new ResourceNotFoundException("Custom Shape by name " + name + " Not Found"));
        return boardMapper.customShapeDAOtoshape(shapeDAO);
    }

    @Override
    public CustomShapeDTO deleteByName(String name) {
        CustomShapeDAO shapeDAO = Optional.ofNullable(customShapeRepository.getbyName(name))
                .orElseThrow(() -> new ResourceNotFoundException("Custom shape with name " + name + " not Found"));
        customShapeRepository.deleteByName(name);
        return boardMapper.customShapeDAOtoshape(shapeDAO);
    }

    @Override
    public List<CustomShapeDTO> getAll() {
        List<CustomShapeDAO> shapeDAOs = customShapeRepository.getAll();
        if (shapeDAOs.isEmpty()) {
            return new ArrayList<CustomShapeDTO>();
        }
        return boardMapper.customshapeListDAOtoDTO(shapeDAOs);
    }
}
