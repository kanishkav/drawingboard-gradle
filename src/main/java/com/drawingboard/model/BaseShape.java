package com.drawingboard.model;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(use = JsonTypeInfo.Id.DEDUCTION)
@JsonSubTypes({
        @JsonSubTypes.Type(Circle.class),
        @JsonSubTypes.Type(Line.class),
        @JsonSubTypes.Type(CustomShape.class),
})
public class BaseShape {

}
