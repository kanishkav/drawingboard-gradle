package com.drawingboard.model;

public enum Shape {
    CIRCLE("Circle"),
    CUSTOM("CustomShape"),
    LINE("Line");

    private final String name;

    Shape(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public static Shape getEnum(String value) {
        for (Shape shape : values()) {
            if (shape.getName().equalsIgnoreCase(value)) {
                return shape;
            }
        }
        return null;
    }
}
