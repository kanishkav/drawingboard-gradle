package com.drawingboard.model;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@ToString
@AllArgsConstructor
public class Circle extends BaseShape {

    // Fields
    private double radius;
}
