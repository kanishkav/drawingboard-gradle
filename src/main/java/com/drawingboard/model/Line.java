package com.drawingboard.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Line extends BaseShape {

    private Point endPoint;
}
