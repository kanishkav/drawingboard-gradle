package com.drawingboard.model;

import com.drawingboard.dto.ShapeWrapperMinimalDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class CustomShape extends BaseShape {
    private String name;
    private List<ShapeWrapperMinimalDTO> shapes;
}
