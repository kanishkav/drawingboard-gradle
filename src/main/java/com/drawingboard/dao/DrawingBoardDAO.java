package com.drawingboard.dao;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class DrawingBoardDAO {

    //Fields
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, name = "id")
    private int id;

    @Column(nullable = false, name = "width")
    private int width;

    @Column(nullable = false, name = "height")
    private int height;

    @Builder.Default
    @Column(nullable = false, name = "backgroundcolor")
    private String backgroundColour = "White";

    @Builder.Default
    @Column(nullable = false, name = "zoom")
    private double zoom = 1;

    @Builder.Default
    @Column(nullable = false, name = "panvertical")
    private double panVertical = 0;

    @Builder.Default
    @Column(nullable = false, name = "panhorizontal")
    private double panHorizontal = 0;

}
