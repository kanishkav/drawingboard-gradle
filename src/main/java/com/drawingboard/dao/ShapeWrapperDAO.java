package com.drawingboard.dao;

import com.drawingboard.model.Shape;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
public class ShapeWrapperDAO {

    // Fields
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(nullable = false, name = "color")
    private String backGroundColor;

    @Column(nullable = false, name = "linethickness")
    private double lineThickness;

    @Column(nullable = false, name = "startx")
    private double startX;

    @Column(nullable = false, name = "starty")
    private double startY;

    @Column(nullable = false, name = "selected")
    private Boolean selected;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false, name = "shapename")
    private Shape shapeName;

    @Column(nullable = false, name = "attributes", columnDefinition = "MEDIUMTEXT")
    private String attributes;

    @ManyToOne
    @JoinColumn(nullable = false, name = "drawingboard_id")
    @JsonBackReference
    private int board_id;


}
