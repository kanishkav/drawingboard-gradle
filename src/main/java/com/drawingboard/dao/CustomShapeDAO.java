package com.drawingboard.dao;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Id;

@Getter
@Setter
@NoArgsConstructor
public class CustomShapeDAO {
    @Id
    @Column(nullable = false, name = "name")
    private String name;

    @Column(nullable = false, name = "shapes", length = 1500)
    private String shapes;
}
