CREATE TABLE shapes
(
    id              INT AUTO_INCREMENT NOT NULL,
    color           VARCHAR(255)       NOT NULL,
    linethickness   DOUBLE             NOT NULL,
    startx          DOUBLE             NOT NULL,
    starty          DOUBLE             NOT NULL,
    selected        BIT(1)             NOT NULL,
    shapename       VARCHAR(255)       NOT NULL,
    attributes      VARCHAR(255)       NOT NULL,
    drawingboard_id INT                NOT NULL,
    CONSTRAINT pk_shapes PRIMARY KEY (id)
);

ALTER TABLE shapes
    ADD CONSTRAINT FK_SHAPES_ON_DRAWINGBOARD FOREIGN KEY (drawingboard_id) REFERENCES drawingboard (id);