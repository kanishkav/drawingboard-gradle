CREATE TABLE customshapes
(
    name   VARCHAR(255)  NOT NULL,
    shapes VARCHAR(1500) NOT NULL,
    CONSTRAINT pk_customshapes PRIMARY KEY (name)
);