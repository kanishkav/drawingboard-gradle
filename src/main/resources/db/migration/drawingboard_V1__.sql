CREATE TABLE drawingboard
(
    id              INT AUTO_INCREMENT NOT NULL,
    width           INT                NOT NULL,
    height          INT                NOT NULL,
    backgroundcolor VARCHAR(255)       NOT NULL,
    zoom            DOUBLE             NOT NULL,
    panvertical     DOUBLE             NOT NULL,
    panhorizontal   DOUBLE             NOT NULL,
    CONSTRAINT pk_drawingboard PRIMARY KEY (id)
);