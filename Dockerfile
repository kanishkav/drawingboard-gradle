FROM openjdk:11.0.12

VOLUME /tmp

RUN mkdir -p /home/app

COPY ./build/libs/*.jar /home/app

ENTRYPOINT ["java","-jar","./home/app/drawingboard-0.1.jar"]